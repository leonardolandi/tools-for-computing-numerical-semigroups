extends TileMap

# Colour enums, corresponding to tileset IDs
enum { dot, white, gray, black, red, blue, green, pink, yellow, purple, cyan, orange, darkblue, lime }

# Other constants
var default_colour := red
var savepath = "user://" + conf.savefile + ".save"
var multiplicity = conf.apery[1]
var split = multiplicity
var savestate := {}
var label

func _enter_tree():
	
	# Generate a new save file if it does not exist
	var file := File.new()
	if not file.file_exists(savepath):
		var error := file.open(savepath, File.WRITE)
		# Unknown error
		if error != OK:
			print("Failed opening save file. Error " + str(error))
			return
		# No error
		file.store_string(to_json(savestate))
		file.close()
		print("Created new save file.")

func _ready():
	
	label = get_tree().get_root().find_node("Label", true, false)
	
	# Update split
	if conf.split in range(1,multiplicity): split = conf.split
	
	# Draw Apery set
	for i in conf.apery:
		var colour := white
		if i in conf.generators: colour = black
		var cell := int_to_cell(i)
		set_cell(cell[0], cell[1], colour)
	
	# Load save file update the Apery set
	var file := File.new()
	var error := file.open(savepath, File.READ)
	# Unknown error
	if error != OK:
		print(error, "Failed opening save file")
		return
	# No error
	savestate = parse_json(file.get_as_text())
	file.close()
	
	# Update with saves
	for key in savestate:
		var cell := int_to_cell(int(key))
		set_cell(cell[0], cell[1], int(savestate[key]))
	
	# Update split
	if conf.split in range(1,multiplicity): split = conf.split

func _unhandled_input(event):
	
	if event is InputEventMouseMotion:
		var tile := world_to_map(get_global_mouse_position())
		var i = tile.x + tile.y*multiplicity
		if not tile.x in range(split-multiplicity,split) or not i in conf.apery: label.text = ""
		else: label.text = str(i)

	elif event is InputEventMouseButton:
		var tile := world_to_map(get_global_mouse_position())
		var i = tile.x + tile.y*multiplicity
		if not tile.x in range(split-multiplicity,split) or not i in conf.apery: return
		if event.button_index == BUTTON_LEFT:
			set_cell(tile.x, tile.y, default_colour)
			savestate[str(i)] = default_colour
			save_on_file()
		elif event.button_index == BUTTON_RIGHT:
			var colour := white
			if i in conf.generators: colour = black
			set_cell(tile.x, tile.y, colour)
			savestate.erase(str(i))
			save_on_file()
	
	elif event is InputEventKey:
		match event.scancode:
			# Numbers
			KEY_0: default_colour = red
			KEY_1: default_colour = blue
			KEY_2: default_colour = green
			KEY_3: default_colour = pink
			KEY_4: default_colour = yellow
			KEY_5: default_colour = purple
			KEY_6: default_colour = cyan
			KEY_7: default_colour = orange
			KEY_8: default_colour = darkblue
			KEY_9: default_colour = lime
			# Keypad numbers
			KEY_KP_0: default_colour = red
			KEY_KP_1: default_colour = blue
			KEY_KP_2: default_colour = green
			KEY_KP_3: default_colour = pink
			KEY_KP_4: default_colour = yellow
			KEY_KP_5: default_colour = purple
			KEY_KP_6: default_colour = cyan
			KEY_KP_7: default_colour = orange
			KEY_KP_8: default_colour = darkblue
			KEY_KP_9: default_colour = lime

func int_to_cell(i: int) -> Array:
	var x = i % multiplicity
	if x >= split: x -= multiplicity
	var y = i / multiplicity
	if x < 0: y += 1
	return [x,y]

func int_ceil(x: int, y:int) -> int: return int(ceil((x+0.0)/y))

func save_on_file():
	var file := File.new()
	var error := file.open(savepath, File.WRITE)
	# Unknown error
	if error != OK:
		print(error, "Failed opening save file")
		return
	# No error
	file.store_string(to_json(savestate))
	file.close()

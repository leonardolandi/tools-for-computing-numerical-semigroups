extends Camera2D

const speed = 20.0
const zoom_speed = 1
const default_zoom = Vector2(50.0,50.0)

func _ready():
	zoom = default_zoom

func _process(_delta):
	
	# Update zoom
	var input_zoom := int(Input.is_action_pressed("ui_page_down")) - int(Input.is_action_pressed("ui_page_up"))
	
	var new_zoom = zoom.x + input_zoom * zoom_speed
	zoom.x = new_zoom
	zoom.y = new_zoom

	# Update position
	var input_x := int(Input.is_action_pressed("ui_right")) - int(Input.is_action_pressed("ui_left"))
	var input_y := int(Input.is_action_pressed("ui_down")) - int(Input.is_action_pressed("ui_up"))
	
	position.x += input_x * speed * new_zoom
	position.y += input_y * speed * new_zoom

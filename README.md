## About

A collection of scripts for studying numerical semigroups.

## The *Godot* tool

The [Godot](./Godot) folder contains a *godot* project for displaying numerical semigroups nicely. For more information on the *godot* game engine see [the official godot website](https://godotengine.org).

To use it, just edit [conf.gd](./Godot/conf.gd) according to the numerical semigroup you want to display and run [project.godot](./Godot/conf.gd) with *godot*.

![](./Godot/screenshot.png)

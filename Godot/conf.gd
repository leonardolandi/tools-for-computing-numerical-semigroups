extends Node

var apery := [] # Insert here the apery set of the semigroup or the semigroup itself
var generators := [] # Insert here the generators of the semigroup or leave empty if you don't know
var split := 0 # Insert here the splitting behaviour of the picture
var savefile := "Apéry" # Insert here the identification name of the file where to save the state

/*
  Find generators of a given semigroup.

  Insert the semigroup elements between the curly brackets in the "semigroup" variable.
  Elements must be ordered!

  Compile with: gcc GeneratorsFromSemigroup.c -o GeneratorsFromSemigroup
  Run with: ./GeneratorsFromSemigroup
*/

const unsigned int semigroup [] = {}; // Set here the semigroup (0 excluded)

// DO NOT EDIT BELOW

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

// MAIN
int main (int argc, char *argv[]) {
  
  unsigned int i, j, k, n, m;
  unsigned int size = sizeof(semigroup) / sizeof(unsigned int);
  unsigned int max = semigroup[size-1];

  // Initialize auxiliary vector
  bool H [size];
  for (i=0; i<size; i++) { H[i] = false; }

  for (i=0; i<size; i++) {

    n = semigroup[i];

    // A generator is found
    if (H[i] == false) {
      H[i] = true;
      printf("%i, ", n);
    }

    // Create the other elements obtainable from n
    for (j=0; j<size; j++) {
      m = n + semigroup[j];
      while (m < max+1) {
	for (k=0; k<size; k++) {
	  if (semigroup[k] == m) { break; }
	}
  	H[k] = true;
  	m += n;
      }

    }
  }

  // Return
  printf("\n");
  return 0;
  
}

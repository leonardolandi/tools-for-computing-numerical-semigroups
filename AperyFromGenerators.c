/*
  Find the Apery set of a semigroup generated by a given set of elements.

  Insert the generating elements between the curly brackets in the "generators" variable.

  Insert the conductor of the semigroup in the "conductor" variable.

  Insert the file name where to print the output in the "file" variable.

  Compile with: gcc AperyFromGenerators.c -o AperyFromGenerators
  Run with: ./AperyFromGenerators
*/

const unsigned int generators [] = {}; // Set here the semigroup generators (0 excluded)
const unsigned int conductor = 0; // Set here the expected conductor of the semigroup
char file [] = "apery.txt"; // Set here the name of the file where to write on

// DO NOT EDIT BELOW

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

// MAIN
int main (int argc, char *argv[]) {
  
  unsigned int i, j, n;
  unsigned int size = sizeof(generators) / sizeof(unsigned int);

  // Initialize H
  bool H [conductor+1];
  for (i=0; i<conductor+1; i++) { H[i] = false; }

  // Add generators to H
  for (i=0; i<size; i++) { H[generators[i]] = true; }
  
  for (i=0; i<conductor+1; i++) {

    printf("\r%i %%", i*100/conductor);

    // Skip 0 values
    if (H[i] == false) { continue; }
    
    for (j=0; j<conductor+1; j++) {

      // Skip 0 values
      if (H[j] == false) { continue; }

      n = i + j;
      while (n < conductor+1) {
  	if (H[n] == false) { H[n] = true; }
  	n += i;
      }

    }
  }

  // Remove non Apery set elements
  for (i=conductor; i>=generators[0]; i--) {
    if (H[i] == true && H[i - generators[0]] == true) { H[i] = false; }
  }

  FILE * f = fopen(file, "w");

  // Print the semigroup
  fprintf(f, "0,");
  for (i=0; i<conductor+1; i++) {
    if (H[i] == true) { fprintf(f, "%i,", i); }
  }
  fprintf(f, "%i", conductor-1+generators[0]);

  // Return
  fclose(f);
  printf("\rResult printed on %s.\n", file);
  return 0;
  
}
